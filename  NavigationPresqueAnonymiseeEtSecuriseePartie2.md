# UNE NAVIGATION(PRESQUE) ANONYME ET SÉCURISÉE - PARTIE 2

Tutoriel testé sur Windows 10 et Gnu/Linux Emmabuntüs DE 3

La deuxième partie de ce tutoriel va expliquer comment échanger des données chiffrés (messages, mails, fichiers) sur Internet.

## Les messageries instantanée.

Quelques messageries instantanées chiffrent de bout en bout les messages qu'on échange avec son interlocuteur : Signal, WhatsApp, Telegram, Wire, Olvid. Elle sont disponibles sur Android, IOS et la plupart des systèmes d'exploitation pour ordinateur. Dans celles citées précédemment seule Signal et Olvid sont totalement des logiciels libres. Pour Telegram seule l'application client est un logiciel libre. Récemment WhatsApp à modifier ses conditions d'utilisation pour permettre à Facebook d'exploiter les données de ses utilisateurs.

## 7-zip

7-zip est un logiciel de compression et de chiffrement des documents recommandé par la CNIL (la Commission Nationale de l'Informatique et des Libertés). 7-zip va créer fichier de type archive. C'est-à-dire un fichier compressé contenant un ou plusiers fichiers et/ou dossiers que vous avez chois d'y mettre. 7-Zip va en plus chiffrer cette archive : l'archive ne pourra être ouverte qu'à l'aider d'un mot de passe. Le chiffrement AES-256 garanti que sans le mot de passe ouvrir l'archive est (pour l'instant) impossible. Ce logiciel est simple à installer et utiliser mais il n'est que partiellement libre. Il est gratuit, librement diffusable mais tout son code source n'a pas été rendu public. Néanmoins c'est une solution simple et rapide pour ceux qui veulent chiffrer des documents mais qui ne sont pas très à l'aise avec le côté technique de l'informatique. Il faut simplement veiller à ce que le mot de passe ne soit facile à deviner et qu'il ne soit communiquer qu'aux personnes autorisées à ouvrir l'archive. Vous pouvez utiliser un générateur de mot de passe aléatoire comme celui-ci : https://www.motdepasse.xyz/.

### 7-Zip avec Windows 

vous pouvez télécharger 7-Zip à cette addresse : https://www.7-zip.org/. Une fois le téléchargement fait il suffit de double cliquer sur le fichier et la procédure d'installation sera la même que la plupart de celles des logiciels sous Windows. Pour chiffrer des documents :

- selectionner tous les fichiers et dossiers que voulez chiffrer,

- faire un clique droit, choisir « 7-Zip » et « Ajouter à l'archive », 

- dans la fenêtre qui va s'afficher, à « Archive » choisir le nom de l'archive qui va être créée et dossier dans laquelle elle sera déposée, 

- à « Format de l'archive » choisir  « 7z », 

- à « Méthode de chiffrement » choisir AES-256 », 

- renseigner un mot de passe et le confirmer, 

- cliquer sur « chiffrement les noms de fichiers », 

- si vous voulez que les fichiers originaux soient effacé une fois que l'archive est créée cliquer sur « Effacer les fichiers après chiffrement », 

- cliquer sur « OK »,

- vérifier qu'il y a bien fichier .7z du nom que vous lui avez donné dans le dossier choisi précédemment.
 

Pour ouvrir l'archive :

- faire un clique droit, choisir « 7-Zip » et « Ouvrir archive », 

- entrer le mot de passe choisi pour le chiffrement,

- cliquer sur « Extraire » et choisir le dossier dans lequel seront déposé les fichiers contenus dans l'archive,

- cliquer sur « OK ».

- vérifier que les fichiers sont bien dans le dossier choisi précédemment.
        

### 7-Zip avec Linux Debian/Ubuntu

Si 7-Zip n'est pas préinstallé l'installer en lançant la commande suivante avec le super-utilisateur.

        apt-get install p7zip-full

Pour compresser et chiffrer un ou des fichiers et dossiers se placer dans le dossier dans lequel il se trouve et lancer la commande suivante.

        7z a -t7z -m0=lzma2 -mx=9 -mfb=64 -md=32m -ms=on -mhe=on -p'motdepasse' monarchive.7z fichier1 fichier2 dossier1 dossier2

motdepasse : mot de passe que vous avez choisi.

fichier1 fichier2 dossier1 dossier2 : le ou les fichiers et dossiers que vous voulez chiffrer.

monarchive : nom que vous avez choisi pour nommer l'archive.

L'archive créée sera dans le dossier depuis lequel vous lancer la commande. Si vous voulez qu'elle soit dans un autre dossier vous pouvez précder le nom du fichier par le chemin de ce dossier. Exemple : /user/monarchive.7z

Pour décompresser et déchiffrer une archive se placer dans le dossier dans lequel elle se trouve et lancer la commande suivante.

        7z e -p'motdepasse' monarchive.7z



