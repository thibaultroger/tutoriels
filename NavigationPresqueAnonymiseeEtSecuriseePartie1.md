# UNE NAVIGATION(PRESQUE) ANONYME ET SÉCURISÉE - PARTIE 1

Tutoriel testé sur Windows 10 et Gnu/Linux Emmabuntüs DE 3

Le développement et la démocratisation des communications électroniques vont de pair avec le développement de la surveillance électronique. Heureusement le monde des logiciels libres et open source nous offre des ressources
pour une navigation sur Internet presque anonyme et sécurisée. Je précise "presque" car en informatque comme dans les autres domaines le risque zéro n'existe pas. Une faille, technique ou humaine, n'est jamais à exclure. Mais en applicant plusieurs mesures de sécurité on va réduire ce risque. Le traçage de votre connexion sera tellement complexe qu'il faudra une vraie bonne raison pour déployer les moyens nécessaires afin de le faire (et sans garantie de réussite) et en plus les données que vous échangerez seront indéchiffrables.

## Prérequis

* Disposer d'un ordinateur avec Windows 7,8 ou 10 ou une distribution Gnu/Linux Debian ou Ubuntu
* Disposer d'une connexion Internet.
* Avoir au minimum les connaissances de base en informatique.

## Définitions techniques

**Distribution GNU/Linux** : Une distribution GNU/Linux est un système d'exploitation basé sur les composants GNU et le noyau Linux. La majorité sont éditées en tant que logiciel libre. Beaucoup sont comptatibles entre elles. On les regroupes souvent génériquement sous le terme de Linux.
**Adresse IP (Internet Protocol)** : l'addresse IP est le numéro d'identification de chaque machine connectée à un réseau utilisant l'Internet Protocol.

**Chiffrement des données** : chiffrer les données consiste à les rendre seulement lisibles par les machines possédant la clef requise pour le déchiffrement.

**HTTPS (Hypertext Transfer Protocol Secure)**: HTTPS est un protocole qui chiffre la transmission de données entre votre machine et le serveur sur lequel vous vous connectez. Le navigateur web créé une clef de chiffrage temporaire et unique qu'il transmet au serveur pour chiffrer leurs échanges de données.

**Code source** : le code source est le code qui décrit dans un langage informatique (C, Java, PHP, etc.) les instructions permettant de faire fonctionner un programme informatique.

**Logiciel libre** : Un logiciel libre est un logiciel édité sous une licence qui autorise de l'utiliser, le copier, le diffuser et le modifier librement, il est donc gratuit et son code source doit être en libre accès, et qui interdit de privatiser son code source. Tout logiciel propriétaire utilisant le code source d'un logiciel libre doit publier cette partie du code et ne peut prétendre à des droits sur celle-ci.

**Logiciel Open Source** : Un logiciel Open Source est un logiciel édité sous une licence qui impose le libre accès au code source mais qui ne le rend pas forcément libre d'utilisation, de diffusion, de copie et de modification.

**Logiciel propriétaire** : Un logiciel propriétaire est un logiciel souvent payant, souvent édité sous une licence n'autorisant pas sa libre diffusion et sa libre copie et dont le code source n'est pas en libre accès.

**GNU/Linux** : GNU/Linux, plus souvent orthographié Linux, est un noyau de système d'exploitation édité sous la licence de logiciel libre GPL.

**VPN (Virtual Private Network)** : Un VPN est un service qui permet de naviguer sur Internet de façon anonymisée et sécurisée. La navigation Internet passer par un tunnel entre votre machine et un serveur VPN. Dans ce tunnel les données échangées sont chiffrées. Les pages web et les applications sur lesquelles on se connecte enregistrent les données d'identification du serveur VPN (adresse IP, géolocalisation) et non celles de votre machine.

**Serveur VPN** : un serveur VPN est un serveur informatique qui héberge et fournit les application permettant de créer un VPN et de s'y connecter.

**Client VPN** : un client VPN est un logiciel qui permet de connecter une machine (ordinateur, smartphone, tablette) à serveur VPN. Le client OpenVPN est un logiciel libre.

**OpenVPN** : OpenVPN désigne à la fois un protocole et un client permettant de connecter une machine à un serveur VPN.

**GAFAM** : GAFAM est l'acronyme des cinq géants d'Internet : Google, Apple, Facebook, Amazon, Microsoft.

**BATX** : BATX est l'acronyme des cinq géants chinois d'Internet : Baidu, Alibaba, Tencent, Xiaomi.

**NATU** : NATU est l'acronyme de quatre grandes entreprises considérées comme symbolique de l'uberisation de l'économie numérique : Netflix, AirBnB, Tesla, Uber.





## Services éthiques et logiciels libres

### Services éthiques

Une grande part de l'activité commerciale des GAFAM, BATX, NATU et autres entreprises proposant des services sur les mêmes modèles se base sur la collecte et l'exploitation des données de navigation de leurs utilisateurs, y compris les données privées. Les lois de divers pays les oblige à communiquer ces données aux autorités publiques et quelques scandales ont démontré qu'ils utilisent ces données bien plus largement que ce qu'ils déclarent. Pour protéger ses données privées une des premières choses à faire est d'utiliser des services numériques qui assurent un usage éthique de vos données : leurs chartes garantissent le chiffrage de vos données privées et de ne pas exploiter vos données et de ne pas utiliser de traqueurs numériques sans au moins vous en demandez l'autorisation et vous donnez des précisions sur leur utilisation. La plupart de ses services se basent sur des logiciels libres. Vous pouvez donc connaître le fonctionnement des technologies qu'ils utilisent. Vous trouverez à ces adresse des services éthiques et libres qui sont de très bonnes alternatives aux services propriétaires et non-éthiques : https://framalibre.org/ , https://degooglisons-internet.org/fr/ , https://entraide.chatons.org/fr/ .   

### Logiciels libres

Comme nous l'avons vu dans les définitions on ne connait pas les codes sources des logiciels propriétaires. On ne peut donc pas connaître la totalité de leur fonctionnement, ce qui inclut les données qu'ils envoient sur Internet et celles qui télécharges sur votre machine. Par exemple certains, comme Windows, lancent des mises à jour sans demander l'autorisation à l'utilisateur et sans préciser quels seront les données téléchargées et les changements effectués. Vous trouverez à ces adresse des logiciels libres qui couvrent la plupart des usages informatiques habituels : https://framalibre.org/ , https://degooglisons-internet.org/fr/ , https://comptoir-du-libre.org/fr/ .

Un bon usage est de préfération l'utilisation d'un système d'exploitation libre à un système d'exploitaiton propriétaires comme Windows et MacOS pour ne citer que les deux plus connus. Dans les systèmes d'exploitations libres  il y a principalement Linux et les systèmes OpenBSD et FreeBSD. Ils sont plus stables et plus sécurisés que les systèmes d'exploitation propriétaires. Un autre avantage est qu'il existe peu de virus informatiques pour ces systèmes. Vous trouverez sur ce site la liste, l'actualité, le classement des systèmes d'exploitation par nombre de téléchargement et les liens vers les sites officiels de tous les systèmes d'exploitation que le site a pu répertorier : https://distrowatch.com/ .

[FreeBSD](https://www.freebsd.org/fr/) et [OpenBSD](https://www.openbsd.org/) sont des systèmes d'exploitation parmis les plus sécurisés et les plus stables. Plus conçus pour être utilisés sur un serveur que sur un ordinateur de bureau leur utilisation est difficile pour les débutants. Néanmoins FreeBSD est bien documentée et il en existe un dérivé, [GhostBSD](https://ghostbsd.org/), qui est optmisé pour l'utilisation de bureau et avec les logiciels les plus utilisés sour FreeBSD préinstallés afin de faciliter son installation et son utilisation.

Il y a de nombreuses distributions Linux, des plus techniques aux plus simples à utiliser. Il y en a qui sont optimisées pour une navigation sécurisée. [Tails](https://tails.boum.org/index.fr.html) est une distribution Live basée sur la distribution Debian. Elle est parametrée pour ne laisser aucune trace sur l'ordinateur et préinstalle des applications permettant une navigation anonymisée et des communications électroniques sécurisées. [QubesOS](https://www.qubes-os.org/) est une distribution orientée sur la sécurité. Toutes les applications sont isolées dans des VM (nous reviendrons sur ce principe dans ce tutoriel) afin que d'eventuels dysfonctionnements ou piratages n'affectent pas l'ensemble du système. Cela la rend très sécurisée mais difficile d'installation et d'utilisation pour les débutants. [Emmabuntüs](https://emmabuntus.org/) est une distribution Linux basée sur Debian. C'est celle que je recommande pour les débutants sur Linux. Elle est optimisée pour faciliter le reconditionnement des ordinateur (une version est généralement utilisable sur une ordinateur qui a jusqu'à une quinzaine d'année à la date de sa sortie) et l'utilisation par les débutant. Des logiciels, notamment ceux développés et/ou promus par l'association [Framasoft](https://framasoft.org/fr/), permettant une navigation internet respectueuse des données privées et des communications électroniques sécurisées sont préinstallés. Pour chaque version la liste des tous les logiciels préinstallés est fournie sur le site officiel. La documentation officielle, disponible sur le site, explique de manière simple l'installation et l'utilisation. Le fichier d'installation d'Emmabuntüs permet une utilisation Live afin la tester sans avoir l'installer sur son ordinateur.


## Tor

Le moyen le plus facile pour avoir une navigation anonymisée et chiffrée est d'utiliser le navigateur Tor (Tor Onion Routing). Tor est un navigateur web basé sur Firefox. Il est paramétré par défaut pour laisser le moins de trace possible de votre navigation (historique automatiquement effacé après chaque utilisation, blocage des trackeurs usuels). IL fait passé votre chemin navigation par un réseau décentralisé de serveurs. Chaque serveur est un noeud du réseau Tor et est mis à disposition bénévolement par un utilisateur du réseau. Entre votre ordinateur et le site que vous voulez visiter votre connexion passe par au moins 3 noeuds. L'adresse IP que le site enregistre sera celle du dernier noeud ce qui permet de masquer votre addresse IP. La connexion entre votr machine et le premier noeud et entre tous les noeuds est chiffrée. Celle entre le dernier serveur et le site que vous consultez l'est si ce dernier a activé le protocole HTTPS. Si ce n'est pas le cas Tor vous prévient. L'extension HTTPS Everywhere est installée directement sur le navigateur Tor ce qui permet d'utiliser le protocole HTTPS à chaque fois que cela est possible.
 
### Installer Tor
Aller à l'URL suivante et télécharger une version de Tor selon votre système de navigation : https://www.torproject.org/download/  (il est aussi disponible sur Android, Tor Browser, et IOS, Onion Browser). Pour Windows cliquer sur le fichier télécharger. L'installation se fera comme tout autres logiciels. Pour Linux extraire l'archive dans un répertoire. 

Dans le repertoire où est installé Tor, ou sur votre bureau ou dans le menu démarrer pour Windows, cliquer sur l'icone Start Tor Browser ou Setup Tor Browser.

Une fois le navigateur lancé cliquer sur Connect ou sur Configure si vous êtes dans un pays où Internet est censuré ou si vous utilisé un proxy. Pour le premier choix vous pouvez utiliser le navigateur il est connecté au réseau Tor. Dans le cas d'une connexion depuis un pays où Internet est censuré il faut selectionne un pont. Dans le cas d'une connexion avec un proxy il faur renseigner les informations de celui-ci.

Vous pouvez vérifier que vvouq naviguer sur Internet avec une nouvelle adresse IP en allant sur http://www.mon-ip.com/ , site qui indique votre adresse IP, avec votre navigateur web habituel et avec Tor.

### Utiliser Tor
Pour une utilisation plus aisée vous pouvez mettre Tor en français. Cliquer l'icone la plus à gauche de la barre d'addresse, aller dans Preferences et dans la rubrique Language choisir « French » et cliquer sur « Appliquer et rédemarrer ».

Quand vous êtes sur une page web vous pouvez voir le chemin emprunter par votre connexion sur le reseau Tor en cliquant sur le cadena à gauche de la barre d'addresse. Vous pouvez modifier ce chemin à tout moment, et ainsi brouiller la piste un peu plus, en cliquant sur « Nouveau circuit pour ce site ».

En cliquant su l'icône en forme de bouclier à droite de la barre d'addresse vous pouvez définir le niveau de sécurité du navigateur. Ces niveaux correspondent au filtrage des traqueurs et scripts pouvant compromettre votre anonymat et la sécurité de votre connexion. 

### Inconvénients
Certains sites sont inaccessibles. Ceux utilisant le traçage et la collecte de données (réseaux sociaux, e-commerce, quelques moteurs de recherches, etc.) rejette les connexions provenant du réseau Tor et les paramétrages de sécurité du navigateur Tor.

La navigation est parfois assez lente.

Le réseau Tor se basant sur le volontariat bénévole n'importe qui peut installer un noeud et en espionner le trafic s'il est noeud de sortie vers un site n'utilisant pas HTTPS.

## VPN

Un VPN peut-être une mesure alternative ou complémentaire à Tor. Alternative si l'on n'aime pas le navigateur Tor, complémentaire si l'on veut ajouter une couche supplémentaire d'anonymat et de chiffrement à sa connexion.

Il existe de nombreux fournisseurs de VPN. Beaucoup sont payants et quelques uns gratuits. Les payants sont souvent plus performants : meilleur anonymat, connexion plus rapide. Parmi les VPN les plus connus il y a [NordVPN](https://nordvpn.com/fr/), ExpressVPN(https://www.expressvpn.com/fr) et [CyberGhost VPN](https://www.cyberghostvpn.com/fr_FR/). 

Pour ce tutoriel nous allons utiliser le client [OpenVPN](https://www.vpnbook.com/) et le VPN gratuit [VPNBook](https://www.vpnbook.com/).

### Installer OpenVPN

Aller à l'URL suivant pour télécharger une version d'OpenVPN en fonction de votre système d'exploitation : https://openvpn.net/download-open-vpn/

Pour Windows le téléchargement d'un fichier Package Windows Installer va se lancer. Une fois le téléchargement fait il suffit de double cliquer sur le fichier et la procédure d'installation sera la même que la plupart de celles des logiciels sous Windows.

Pour Linux il faut suivre les instructions fournies en clique sur le bouton « [INSTALL OPENVPN 3 FOR LINUX](https://community.openvpn.net/openvpn/wiki/OpenVPN3Linux) ». Ce sont les mêmes que pour beaucoup de logiciel fonctionnant sous Linux. C'est-à-dire installer le dépôt selon la distribution Linux utilisée, puis installer le client OpenVPN avec le gestionnaire de paquets. Si le paquet openvpn3 n'existe pas essayer de lancer l'installation du paquet openvpn.

OpenVPN est disponible dans l'App Store pour IOS et dans le Play Store pour Android.

### VPNBook

Sur le site de VPNBook aller à la rubrique [Free VPN accounts](https://www.vpnbook.com/freevpn). Cliquer sur un des liens de la colonne « Free OpenVPN » pour télécharger une archive au format zip contenant les certificats de connexion à un des serveur VPN proposer par VPNBook. Décompresser l'archive. Chaque certificat correspond à un protocole et un port sur lesquels il va vous connecter au serveur VPN.

Sur Windows dans OpenVPN cliquer sur le bouton en bas à droite puis « IMPORT FROM FILE ». Choissisez un des certificats et renseigner le nom d'utilisateur (Username) et le mot de passe (Password) qui sont indiqués dans la colonne « Free OpenVPN » de la rubrique « Free VPN accounts ».

Sur Linux se placer dans le répertoire où se trouvent les certificats et lancer la commande suivante.

    openvpn nomducertificat

Si la commande openvpn n'est pas reconnu essayer de la créer en lançant celle-ci avec le super-utilisateur.

    cp /usr/sbin/openvpn /usr/bin

Essayer de lancer à nouveau la commande openvpn.

Le nom d'utilisateur (Username) et le mot de passe (Password) demandés sont ceux indiqués dans la colonne « Free OpenVPN » de la rubrique « Free VPN accounts ».

Vous pouvez vérifier que votre naviguer sur Internet avec une nouvelle adresse IP en allant sur http://www.mon-ip.com/ avant et pendant votre connexion à VPNBook.

## Firefox

Firefox est un navigateur web libre et open source. Son développement est assuré par une communauté de bénévoles et est géré par Mozilla une fondation à but non lucratif. Le respect et la sécurisation des données privés font partie de ces objectifs de son développement. Il offre donc de nombreuses possibilité pour les atteindres.

### navigation privées

Cliquer sur l'icône la plus à droite de la barre d'adresse puis sur « Nouvelle fenêtre privée ». Un nouvelle fenêtre firefox va s'ouvrir. Quand fermerez cette fênêtre toutes les donnée de navigations enregistré par la navigation depuis cette fenêtre seront effacées : historique, cookies, traqueurs.

### Protection renforcée contre le pistage
Cliquer sur l'icône la plus à droite de la barre d'adresse puis sur « Options » et « Vie privée et sécurité ». Dans la rubrique Protection renforcée contre le pistage vous pourrez chosir entre trois types de protection : standard, stricte, personnalisée.

La protection standard bloque tous les scripts et applications de traçage sauf ceux nécessaires au bon fonctionnement du site que vous êtes entrain de visiter.

La protection stricte bloque tous les scripts et applications de traçage. Cela peut vous bloquer l'accès de certains sites qui les impose. En cliquant sur l'icône en forme blouclier à gauche de la barre d'adresse vous pouvez désactivé la protection renforcée pour le site que vous êtes en train de visiter.

La protection personnalisé vous permet de choisir sur quels types de traqueurs et de scripts vous voulez bloquer.

### Extension.

Firefox permet d'installer des extensions. Certaines sont particulièrement utiles et recommandés pour la protection des données privées. Toutes les extensions pour Firefox sur sont sur ce : https://addons.mozilla.org. Vous pouvez trouvez à la rubrique Extensions/Sécurité et vie privée (https://addons.mozilla.org/fr/firefox/search/?category=privacy-security&sort=recommended%2Cusers&type=extension) toutes les extensions utiles à la protection des données privées. Les premières affichées ont le badge « Recommandé », ça veut dire qu'elles sont recommandées par Firefox. Pour installer une extension il suffit de cliquer dessus puis sur le bouton «Ajouter à Firefox ». Nous allons ici en traiter quatre pour avoir une protection des données privées efficace.

#### HTTPS Everywhere

HTTPS Everywhere est une extension qui active le protocole HTTPS à chaque fois que cela est possible. En cliquant sur l'icône en forme de S à à droite de la barre d'addresse vous vous pouvez activé l'option « Chiffrer tous les sites admissibles est activée ». A chaque fois que le protocole HTTPS ne peut pas être activé vous serez prévenu et vous aurez le choix d'aller sur cette page ou non.

#### uBlock Origin

uBlock Origin est une extension qui va bloquer les publicités et certains traqueurs. Si elle vous empêche de consulter un site alors que cela vous est nécessaire vous pouvez la désactivé en cliquant sur l'icone UO dans un blouclier rouge à droite de votre d'addresse, puis sur le bouton d'arrêt.

#### Ghostery – Bloqueur de publicité protégeant la vie

Ghostery est une extension bloquant les publicités et les traqueurs. En cliquant sur l'icone en forme de fantôme bleu à droite de votre barre d'adresse vous pouvez la paramétrer en choissisant ce que vous voulez bloquer et en ajoutant le site que vous consultez à la liste des sites à laquelle toujours se fier (c'est-à-dire ne jamais le bloquer) ou la liste des sites à restreindre (c'est-à-dire toujours bloquer).

#### NoScript Security Suite

NoScript est une extension qui protège des failles de sécurité dues au langage Javascript qui est très utilisé sur de nombreux sites web. 

#### Désactiver une extension
 
 Certains sites sur lesquelles il est nous nécessaires d'aller sont parfois bloqué par ces extensions ou bloquent d'eux-memes l'utilisateur si elles sont activés sur son navigateur. Pour les désactiver (et ensuite les réactiver) cliquer sur l'icône la plus à droite de la barre d'adresse puis sur « Modules complémentaires » et « Extensions ». Vous aurez la liste de vos extensions installées. Vous les désactiver et les réactiver en cliquant sur le bouton en forme d'interrupteur à droite du nom de l'extension. Vous pouvez aussi les supprimer en cliquant suri « ... » à droite du nom de l'extension puis sur « Supprimer ».
